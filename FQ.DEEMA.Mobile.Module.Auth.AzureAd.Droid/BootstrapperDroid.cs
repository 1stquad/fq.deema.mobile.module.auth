﻿using Autofac;

namespace FQ.DEEMA.Mobile.Module.Auth.AzureAd.Droid
{
    public static class BootstrapperDroid
    {
        public static void Configure(ContainerBuilder containerBuilder)
        {
            Bootstrapper.Configure(containerBuilder);

            containerBuilder.RegisterType<LogOutService>().As<IPlatformLogOutService>().InstancePerLifetimeScope();
        }
    }
}