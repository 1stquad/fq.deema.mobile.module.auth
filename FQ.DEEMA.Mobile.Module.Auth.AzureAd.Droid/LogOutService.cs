﻿using Android.Webkit;
using Softeq.XToolkit.WhiteLabel.Threading;

namespace FQ.DEEMA.Mobile.Module.Auth.AzureAd.Droid
{
    public class LogOutService : IPlatformLogOutService
    {
        public void Logout()
        {
            Execute.BeginOnUIThread(() =>
            {
                CookieManager.Instance.RemoveAllCookies(new ValueCallback());
            });
        }

        private class ValueCallback : Java.Lang.Object, IValueCallback
        {
            public void OnReceiveValue(Java.Lang.Object value)
            {
                // ignore
            }
        }
    }
}
