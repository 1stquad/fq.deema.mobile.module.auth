﻿using Android.App;
using Android.Content;
using Android.OS;
using FQ.DEEMA.Mobile.Module.Auth.AzureAd.ViewModels;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Softeq.XToolkit.WhiteLabel.Droid;

namespace FQ.DEEMA.Mobile.Module.Auth.AzureAd.Droid.Views
{
    [Activity]
    public class LoginActivity : ActivityBase<LoginViewModel>
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_login);
            
            ViewModel.StartLoginAsync(new PlatformParameters(this, false, PromptBehavior.Always));
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            AuthenticationAgentContinuationHelper.SetAuthenticationAgentContinuationEventArgs(
                requestCode, resultCode, data);
        }
    }
}