﻿using Autofac;
using FQ.DEEMA.Mobile.Module.Auth.AzureAd.ViewModels;

namespace FQ.DEEMA.Mobile.Module.Auth.AzureAd.iOS
{
    public static class BootstrapperIos
    {
        public static void Configure(ContainerBuilder containerBuilder)
        {
            Bootstrapper.Configure(containerBuilder);

            containerBuilder.RegisterType<LogOutService>().As<IPlatformLogOutService>().InstancePerLifetimeScope();
        }
    }
}