﻿using System;

namespace FQ.DEEMA.Mobile.Module.Auth.AzureAd.iOS
{
    public class LogOutService : IPlatformLogOutService
    {
        public void Logout()
        {
            // TODO YP: unused on this platform
            // (remove cookies here)
        }
    }
}
