using System;
using Softeq.XToolkit.WhiteLabel;
using UIKit;

namespace FQ.DEEMA.Mobile.Module.Auth.AzureAd.iOS
{
    internal static class StyleHelper
    {
        private static readonly Lazy<IAuthStyle> StyleLazy = Dependencies.IocContainer.LazyResolve<IAuthStyle>();

        public static IAuthStyle Style => StyleLazy.Value;
    }

    public interface IAuthStyle
    {
        UIColor AccentColor { get; }
    }
}