﻿using System;
using FQ.DEEMA.Mobile.Module.Auth.AzureAd.ViewModels;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Softeq.XToolkit.WhiteLabel.iOS;

namespace FQ.DEEMA.Mobile.Module.Auth.AzureAd.iOS.ViewControllers
{
    public partial class LoginViewController : ViewControllerBase<LoginViewModel>
    {
        public LoginViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            NavigationController.NavigationBar.Hidden = true;
            ProgressIndicator.Color = StyleHelper.Style.AccentColor;
            
            ViewModel.StartLoginAsync(new PlatformParameters(this, false, PromptBehavior.Always));
        }
    }
}