﻿namespace FQ.DEEMA.Mobile.Module.Auth.AzureAd
{
    public class AuthConfig
    {
        public string Authority { get; set; }
        public string Resource { get; set; }
        public string ClientId { get; set; }
        public string RedirectUri { get; set; }
    }
}