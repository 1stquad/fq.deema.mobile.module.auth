﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using FQ.DEEMA.Mobile.Module.Auth.AzureAd.ViewModels;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Softeq.XToolkit.Auth;
using Softeq.XToolkit.Common.Interfaces;
using Softeq.XToolkit.WhiteLabel.Navigation;

namespace FQ.DEEMA.Mobile.Module.Auth.AzureAd
{
    public class AuthService : IAuthService
    {
        private readonly AuthConfig _authConfig;
        private readonly IAccountService _accountService;
        private readonly ILogger _logger;
        private readonly IPageNavigationService _pageNavigationService;
        private readonly IPlatformLogOutService _logoutService;

        private ICommand _canceledCommand;
        private ICommand _successCommand;

        public AuthService(
            IPageNavigationService pageNavigationService,
            IPlatformLogOutService logOutService,
            ILogManager logManager,
            IAccountService accountService,
            AuthConfig authConfig)
        {
            _pageNavigationService = pageNavigationService;
            _logoutService = logOutService;
            _accountService = accountService;
            _authConfig = authConfig;
            _logger = logManager.GetLogger<AuthService>();
        }

        public IPlatformParameters PlatformParameters { get; set; }

        public void StartLoginProcess(ICommand successCommand, ICommand canceledCommand)
        {
            _successCommand = successCommand;
            _canceledCommand = canceledCommand;
            _pageNavigationService.NavigateToViewModel<LoginViewModel>(true);
        }

        public async Task LoginAsync()
        {
            var result = false;
            var authResult = default(AuthenticationResult);

            try
            {
                var authContext = new AuthenticationContext(_authConfig.Authority);

                authResult = await authContext.AcquireTokenAsync(
                    _authConfig.Resource,
                    _authConfig.ClientId,
                    new Uri(_authConfig.RedirectUri),
                    PlatformParameters);

                _accountService.ResetTokens(authResult.AccessToken, null);

                result = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            if (result)
            {
                _successCommand.Execute(this);
            }
            else
            {
                _canceledCommand?.Execute(this);
            }
        }

        public Task<bool> RegisterAsync(string email, string password)
        {
            throw new NotImplementedException();
        }

        public Task<bool> LoginAsync(string email, string password)
        {
            throw new NotImplementedException();
        }

        public void Logout()
        {
            _logoutService.Logout();

            _accountService.Logout();
        }
    }
}