﻿using Autofac;
using FQ.DEEMA.Mobile.Module.Auth.AzureAd.ViewModels;

namespace FQ.DEEMA.Mobile.Module.Auth.AzureAd
{
    public static class Bootstrapper
    {
        public static void Configure(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterType<LoginViewModel>().InstancePerDependency();
        }
    }
}