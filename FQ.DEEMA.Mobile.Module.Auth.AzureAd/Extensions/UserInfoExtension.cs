﻿using System.Text;
using Microsoft.IdentityModel.Clients.ActiveDirectory;

namespace FQ.DEEMA.Mobile.Module.Auth.AzureAd.Extensions
{
    internal static class UserInfoExtension
    {
        public static string GetDisplayName(this UserInfo userInfo)
        {
            var sb = new StringBuilder();

            if (!string.IsNullOrEmpty(userInfo.GivenName))
            {
                sb.Append(userInfo.GivenName);
            }

            if (!string.IsNullOrEmpty(userInfo.FamilyName))
            {
                if (sb.Length > 0)
                {
                    sb.Append(" ");
                }

                sb.Append(userInfo.FamilyName);
            }

            if (sb.Length == 0)
            {
                sb.Append(userInfo.DisplayableId);
            }

            return sb.ToString();
        }
    }
}