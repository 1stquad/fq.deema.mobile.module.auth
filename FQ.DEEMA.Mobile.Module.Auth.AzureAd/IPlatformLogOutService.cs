﻿namespace FQ.DEEMA.Mobile.Module.Auth.AzureAd
{
    public interface IPlatformLogOutService
    {
        void Logout();
    }
}
