﻿using System;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Softeq.XToolkit.Auth;
using Softeq.XToolkit.Common.Interfaces;
using Softeq.XToolkit.RemoteData;
using Softeq.XToolkit.RemoteData.HttpClient;

namespace FQ.DEEMA.Mobile.Module.Auth.AzureAd
{
    public class RefreshTokenService : RefreshTokenServiceBase
    {
        private readonly IAccountService _accountService;
        private readonly AuthConfig _authConfig;
        private readonly ILogger _logger;

        public RefreshTokenService(
            IAccountService accountService,
            ILogManager logManager,
            AuthConfig authConfig)
        {
            _accountService = accountService;
            _authConfig = authConfig;
            _logger = logManager.GetLogger<RefreshTokenService>();
        }

        protected override async Task<RefreshTokenStatus> TryRefreshToken(IRestHttpClient restHttpClient)
        {
            try
            {
                var authContext = new AuthenticationContext(_authConfig.Authority);

                var authResult = await authContext.AcquireTokenSilentAsync(
                    _authConfig.Resource,
                    _authConfig.ClientId,
                    new UserIdentifier(_accountService.UserId, UserIdentifierType.UniqueId)).ConfigureAwait(false);

                _accountService.ResetTokens(authResult.AccessToken, null);

                return RefreshTokenStatus.Refreshed;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return RefreshTokenStatus.Error;
        }
    }
}