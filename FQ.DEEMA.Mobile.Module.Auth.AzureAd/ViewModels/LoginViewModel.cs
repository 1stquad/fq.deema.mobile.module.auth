﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Softeq.XToolkit.Auth;
using Softeq.XToolkit.Common.Extensions;
using Softeq.XToolkit.WhiteLabel.Mvvm;

namespace FQ.DEEMA.Mobile.Module.Auth.AzureAd.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        private readonly AuthService _authService;
        
        public LoginViewModel(
            IAuthService authService)
        {
            _authService = (AuthService) authService;
        }

        public void StartLoginAsync(PlatformParameters platformParameters)
        {
            _authService.PlatformParameters = platformParameters;
            _authService.LoginAsync().SafeTaskWrapper();
        }
    }
}